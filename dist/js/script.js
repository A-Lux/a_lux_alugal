$(document).ready(function(){
    let serteficate = $('.owl-sertevicate')
    serteficate.owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        dots:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
    $('.next-serteficate').click(function(e) {
        e.preventDefault();
        serteficate.trigger('next.owl.carousel');
    });
    $('.prev-serteficate').click(function(e) {
        e.preventDefault();
        serteficate.trigger('prev.owl.carousel');
    });


    var owl = $('#owl-demo')
    $('#owl-demo').owlCarousel({
        items: 3, 
        loop:true,
        dots:false,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:3
            }
        }
    })

    // Go to the next item
    $('.arrow-right').click(function() {
        owl.trigger('next.owl.carousel');
    })
// Go to the previous item
$('.arrow-left').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl.trigger('prev.owl.carousel', [300]);
})



var owl2 = $('#owl-demo2');
$('#owl-demo2').owlCarousel({
    items: 2, 
    loop:true,
    dots:false,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:2
        }
    }
})

// Go to the next item
$('.arrow_right_letter').click(function() {
    owl2.trigger('next.owl.carousel');
})
// Go to the previous item
$('.arrow_left_letter').click(function() {
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owl2.trigger('prev.owl.carousel', [300]);
})


/* Career page*/
$('.collapsetext, .date, .arrow_vacancy_career').click(function(){
    var value = $(this).parent().parent().attr("value").toString();
    console.log($(this).parent().parent().attr("value"));



    if($(".row[value=" + value + "] .collapsetext").hasClass("collapse-active")){
        $(".row[value=" + value + "] .collapsetext").removeClass("collapse-active");
        $(".row[value=" + value + "] .date").removeClass("date-active");
        $(".row[value=" + value + "] img.float-right").removeClass("direction");

        return;
    } 
    $(".collapsetext").removeClass("collapse-active");
    $(".date").removeClass("date-active");
    $(".row img.float-right").removeClass("direction");

    $(".row[value=" + value + "] .collapsetext").addClass("collapse-active");
    $(".row[value=" + value + "] .date").addClass("date-active");
    $(".row[value=" + value + "] img.float-right").addClass("direction");

    /* End of Career page */
})
var j = 0;

$('.wrapper_career').click(function(){

    console.log("showing elements");
    if(j%2 == 0){
        $(this).children('.texts').css("transform", "scale(1)");
        $(this).children('.x_career').css("transform", "scale(1)");
        $(this).children('.info_career').css("transform", "scale(0)");
        $(this).children('.bg_photo').css("opacity", "0.5");
        j++;
    } else{
        $(this).children('.texts').css("transform", "scale(0)");
        $(this).children(".x_career").css("transform", "scale(0)");
        $(this).children(".info_career").css("transform", "scale(1)");
        $(this).children('.bg_photo').css("opacity", "1");
        j++;

    }
   
});


});

function call_me(){
    alert("Hey");
}


AOS.init({
    easing: 'ease-out-back',
    duration: 1000
});